package configuration

import (
	"os"
	"strings"
)

// Configuration holds the configuration
type Configuration struct {
	SSL        bool
	DBUsername string
	DBPassword string
	DBHost     string
	DBName     string
}

const (
	dbSSL          = "DB_SSL"
	dbUsernameName = "DB_USERNAME"
	dbPasswordName = "DB_PASSWORD"
	dbHostName     = "DB_HOST"
	dbNameName     = "DB_NAME"
)

func get(name string) string {
	return os.Getenv(name)
}

// LoadConfiguration loads the configration from environment variables
func LoadConfiguration() Configuration {
	return Configuration{
		SSL:        strings.EqualFold(get(dbSSL), "true"),
		DBUsername: get(dbUsernameName),
		DBPassword: get(dbPasswordName),
		DBHost:     get(dbHostName),
		DBName:     get(dbNameName),
	}
}
