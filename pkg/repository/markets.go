package repository

import (
	"github.com/labstack/gommon/log"
	"github.com/pkg/errors"
	"gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/api/v0/models"
	"gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/configuration"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const marketCollectionName = "wvw.market"

// MarketDB is used to query or update the market database
type MarketDB interface {
	FindMarket(filter bson.M) ([]models.Market, error)
	AddMarket(market models.Market) error
	AddLocationToMarket(location models.Location, market models.Market) error
	DeleteAll() error
	Reconnect() error
}

// NewMarketDB creates a new mongoDB client
func NewMarketDB(options configuration.Configuration) (MarketDB, error) {
	session, err := initDB(options)
	if err != nil {
		return nil, errors.Wrap(err, "error creating new marketDB")
	}
	if session == nil {
		return nil, errors.New("no database session")
	}

	session.SetMode(mgo.Strong, true)
	session.SetSafe(&mgo.Safe{W: 1})

	return &marketService{
		options: options,
		dbName:  options.DBName,
		session: session,
	}, nil
}

func (s *marketService) Ping() error {
	return s.session.Ping()
}

func (s *marketService) Reconnect() error {
	return reconnect(s.options, s)
}

type marketService struct {
	dbName  string
	session *mgo.Session
	options configuration.Configuration
}

func (s *marketService) setSession(session *mgo.Session) {
	s.session = session
}

func (s *marketService) FindMarket(filter bson.M) ([]models.Market, error) {
	session := s.session.Copy()
	defer session.Close()
	var markets []models.Market

	err := session.DB(s.dbName).C(marketCollectionName).Find(filter).All(&markets)
	if err != nil {
		return nil, errors.Wrapf(err, "error finding market with filter %+v", filter)
	}
	return markets, nil
}

func (s *marketService) AddMarket(market models.Market) error {
	session := s.session.Copy()
	defer session.Close()
	markets, err := s.FindMarket(bson.M{"address": market.Address})
	if err != nil {
		return errors.Wrapf(err, "error checking if market %+v already exists", market)
	}
	if len(markets) > 0 {
		log.Info("Market does already exist")
		return nil
	}
	err = session.DB(s.dbName).C(marketCollectionName).Insert(market)
	if err != nil {
		return errors.Wrapf(err, "error adding new market %+v", market)
	}
	return nil
}

func (s *marketService) AddLocationToMarket(location models.Location, market models.Market) error {
	session := s.session.Copy()
	defer session.Close()
	err := session.DB(s.dbName).C(marketCollectionName).Update(
		bson.M{"address": market.Address},
		bson.M{"$push": bson.M{"locations": location}},
	)
	if err != nil {
		return errors.Wrapf(err, "error adding new location %+v to market %+v", location, market)
	}
	return nil
}

func (s *marketService) DeleteAll() error {
	session := s.session.Copy()
	defer session.Close()
	err := session.DB(s.dbName).C(marketCollectionName).DropCollection()
	if err != nil {
		return errors.Wrap(err, "Fail to drop collection")
	}
	return nil
}
