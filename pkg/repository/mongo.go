package repository

import (
	"crypto/tls"
	"net"
	"strings"
	"time"

	"github.com/labstack/gommon/log"
	"github.com/pkg/errors"

	"gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/configuration"
	"gopkg.in/mgo.v2"
)

// InitDB inits the database
func initDB(options configuration.Configuration) (*mgo.Session, error) {
	dialInfo := &mgo.DialInfo{
		Addrs:    strings.Split(options.DBHost, ","),
		Database: options.DBName,
		Username: options.DBUsername,
		Password: options.DBPassword,
		DialServer: func(addr *mgo.ServerAddr) (net.Conn, error) {
			return tls.Dial("tcp", addr.String(), &tls.Config{InsecureSkipVerify: true})
		},
		ReplicaSetName: "rs0",
		Timeout:        time.Second * 10,
	}

	if !options.SSL {
		dialInfo.ReplicaSetName = ""
		dialInfo.DialServer = nil
	}
	// connect to the database
	session, err := mgo.DialWithInfo(dialInfo)
	if err != nil {
		return nil, errors.Wrap(err, "error initializing DB")
	}
	return session, err
}

func reconnect(options configuration.Configuration, setter sessionSetter) error {
	db, err := initDB(options)
	if err != nil {
		return errors.Wrap(err, "error reconnecting to mongoDB")
	}

	if setter != nil && db != nil {
		setter.setSession(db)
		log.Info("mongodb reconnected!")
	}
	return nil
}

type sessionSetter interface {
	setSession(*mgo.Session)
}
