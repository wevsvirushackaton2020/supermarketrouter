package repository

import (
	"github.com/pkg/errors"
	"gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/api/v0/models"
	"gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/configuration"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// ProductsDB is used to query or update the products database
type ProductsDB interface {
	GetProductByIdentifier(identifier string) (models.Product, error)
	Reconnect() error
}

const (
	productsCollectionName = "products"
)

type productService struct {
	dbName  string
	session *mgo.Session
	options configuration.Configuration
}

func (s *productService) setSession(session *mgo.Session) {
	s.session = session
}

// NewProductsDB creates a new mongoDB client
func NewProductsDB(options configuration.Configuration) (ProductsDB, error) {
	session, err := initDB(options)
	if err != nil {
		return nil, errors.Wrap(err, "error creating new marketDB")
	}

	if session == nil {
		return nil, errors.New("no database session")
	}

	session.SetMode(mgo.Strong, true)
	session.SetSafe(&mgo.Safe{W: 1})

	return &productService{
		options: options,
		dbName:  options.DBName,
		session: session,
	}, nil
}

func (s *productService) Reconnect() error {
	return reconnect(s.options, s)
}

func (s *productService) GetProductsByIdentifier(identifiers []string) ([]models.Product, error) {
	session := s.session.Copy()
	defer session.Close()
	var products []models.Product

	filter := bson.M{
		"identifier": identifiers,
	}

	err := session.DB(s.dbName).C(productsCollectionName).Find(filter).All(&products)
	if err != nil {
		return nil, errors.Wrapf(err, "error finding market with filter %+v", filter)
	}

	return products, nil
}

func (s *productService) GetProductByIdentifier(identifier string) (models.Product, error) {
	session := s.session.Copy()
	defer session.Close()
	product := make([]models.Product, 0)

	filter := bson.M{
		"identifier": identifier,
	}

	err := session.DB(s.dbName).C(productsCollectionName).Find(filter).All(&product)
	if err != nil {
		return models.Product{}, errors.Wrapf(err, "error finding market with filter %+v", filter)
	}

	result := models.Product{}
	if len(product) > 0 {
		result = product[0]
	}

	return result, nil
}
