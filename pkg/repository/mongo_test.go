package repository

import (
	"testing"

	"gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/api/v0/models"
	"gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/configuration"
	"gopkg.in/mgo.v2/bson"
)

func TestAddMarket(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping Integrationtest")
	}

	db, err := NewMarketDB(configuration.LoadConfiguration())
	if err != nil {
		t.Errorf("Failed to create marketDB: %+v", err)
		t.Fail()
	}
	// clear db before test
	err = db.DeleteAll()
	if err != nil {
		t.Errorf("Failed to delete all: %+v", err)
		t.Fail()
	}
	address := models.Address{
		Street:      "Wolfhager Str.",
		HouseNumber: "7",
		ZipCode:     "36123",
		City:        "Kassel",
	}
	market := models.Market{
		Address: address,
		Locations: []models.Location{
			models.Location{
				Position:            2,
				PositionDescription: "Second shelf, right hand side",
				Product: models.Product{
					Name: "Tomato",
					Categories: []models.ProductCategory{
						models.ProductCategory{
							Name: "Fruit",
						},
					},
				},
			},
		},
	}
	err = db.AddMarket(market)
	if err != nil {
		t.Errorf("Failed to add market: %+v", err)
		t.Fail()
	}
	markets, err := db.FindMarket(bson.M{"address": address})
	if err != nil {
		t.Errorf("Failed to find market: %+v", err)
		t.Fail()
	}
	if len(markets) == 0 {
		t.Error("Could not retrieve market by address")
		t.Fail()
	}
}

func TestAddProductToMarket(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping Integrationtest")
	}

	db, err := NewMarketDB(configuration.LoadConfiguration())
	if err != nil {
		t.Errorf("Failed to create marketDB: %+v", err)
		t.Fail()
	}
	// clear db before test
	err = db.DeleteAll()
	if err != nil {
		t.Errorf("Failed to delete all: %+v", err)
		t.Fail()
	}

	address := models.Address{
		Street:      "Wolfhager Str.",
		HouseNumber: "7",
		ZipCode:     "36123",
		City:        "Kassel",
	}
	market := models.Market{
		Address: address,
	}
	err = db.AddMarket(market)
	if err != nil {
		t.Errorf("Failed to add market: %+v", err)
		t.Fail()
	}
	location := models.Location{
		Position:            2,
		PositionDescription: "Second shelf, right hand side",
		Category: models.ProductCategory{
			Name: "Fruits",
		},
	}
	err = db.AddLocationToMarket(location, market)
	if err != nil {
		t.Errorf("Failed to add location to market: %+v", err)
		t.Fail()
	}
	markets, err := db.FindMarket(bson.M{"address": address})
	if err != nil {
		t.Errorf("Failed to find market: %+v", err)
		t.Fail()
	}
	if len(markets) == 0 {
		t.Error("Could not retrieve market by address")
		t.Fail()
	}
	retrievedMarket := markets[0]
	if len(retrievedMarket.Locations) == 0 {
		t.Errorf("Retrieved market does not contain location: %v", retrievedMarket)
		t.Fail()
	}
	categoryName := retrievedMarket.Locations[0].Category.Name
	if categoryName != "Fruits" {
		t.Errorf("Retrieved markets location category is not Fruits: %s", categoryName)
		t.Fail()
	}
}
