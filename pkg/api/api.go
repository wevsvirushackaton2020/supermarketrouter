package api

import (
	"github.com/labstack/echo/v4"

	v0 "gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/api/v0"
	"gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/repository"
	globalServices "gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/services"
)

// API represents the backbone. an HTTP/REST API.
type API interface {
	Register(*echo.Group)
}

// NewV0 returns a new Instance of Api
func NewV0(marketDB repository.MarketDB, productsDB repository.ProductsDB) API {
	return &v0.API{
		MarketDB:   marketDB,
		ProductsDB: productsDB,
		Sanitizer:  globalServices.NewSanitizer(),
	}
}
