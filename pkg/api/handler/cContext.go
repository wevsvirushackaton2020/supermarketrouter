package handler

import (
	"encoding/json"
	"fmt"

	"github.com/gofrs/uuid"
	"github.com/labstack/echo/v4"
	"github.com/neko-neko/echo-logrus/v2/log"
	"github.com/sirupsen/logrus"
)

// CContext is the custom context for requests
type CContext struct {
	echo.Context
	ProcessUUID uuid.UUID
}

// RequestID gets the request ID from the headers
func (cContext *CContext) RequestID() string {
	return cContext.Context.Response().Header().Get(echo.HeaderXRequestID)
}

// LogErr is used for logging errors
func (cContext *CContext) LogErr(externalMessage string, err error, level logrus.Level) {
	go func() {
		log.Logger().
			WithError(err).
			Log(level, externalMessage)
	}()
}

// LogCondition is used to verify that a condition is met before logging
type LogCondition func() bool

// LogIf is used for conditional logging (if condition is met, log; else do nothing)
func (cContext *CContext) LogIf(externalMessage string, level logrus.Level, condition LogCondition) {
	if !condition() {
		return
	}

	cContext.Log(externalMessage, level)
}

// LogJSONIf is used for conditional logging (if condition is met, log; else do nothing)
func (cContext *CContext) LogJSONIf(externalMessage interface{}, level logrus.Level, condition LogCondition) {
	if !condition() {
		return
	}

	message, err := json.Marshal(externalMessage)
	if err != nil {
		cContext.Log(fmt.Sprintf("error while trying to marshall message to json\n%s", err.Error()), logrus.ErrorLevel)
	}

	cContext.Log(string(message), level)
}

// Log is used for general logging
func (cContext *CContext) Log(externalMessage string, level logrus.Level) {
	go func() {
		log.Logger().
			Log(level, externalMessage)
	}()
}
