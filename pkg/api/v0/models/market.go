package models

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

// Market is a struct that stores essential information such as
// Addresses and opening hours
type Market struct {
	Id           bson.ObjectId `json:"id,omitempty" bson:"id,omitempty"`
	OpeningHours TimeTable     `json:"opening_hours,omitempty" bson:"opening_hours,omitempty"`
	Address      Address       `json:"address,omitempty" bson:"address,omitempty"`
	Locations    []Location    `json:"locations,omitempty" bson:"locations,omitempty"`
}

type TimeTable struct {
	DayOfWeek int       `json:"day_of_week,omitempty" bson:"day_of_week,omitempty"`
	Opens     time.Time `json:"opens,omitempty" bson:"opens,omitempty"`
	Closes    time.Time `json:"closes,omitempty" bson:"closes,omitempty"`
}

type Address struct {
	Street      string `json:"street,omitempty" bson:"street,omitempty"`
	HouseNumber string `json:"house_number,omitempty" bson:"house_number,omitempty"`
	City        string `json:"city,omitempty" bson:"city,omitempty"`
	ZipCode     string `json:"zip_code,omitempty" bson:"zip_code,omitempty"`
}

type Location struct {
	PositionDescription string          `json:"position_description,omitempty" bson:"position_description,omitempty"`
	Position            int             `json:"position,omitempty" bson:"position,omitempty"`
	Category            ProductCategory `json:"category,omitempty" bson:"category,omitempty"`
	Product             Product         `json:"product,omitempty" bson:"product,omitempty"`
}
