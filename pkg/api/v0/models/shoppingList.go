package models

type CategorizedShoppingList struct {
	Products []Product
}

type ShoppingList struct {
	Items []string `json:"items,omitempty"`
}
