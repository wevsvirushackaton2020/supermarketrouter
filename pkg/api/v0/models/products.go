package models

import "gopkg.in/mgo.v2/bson"

type ProductCategory struct {
	ID   bson.ObjectId `json:"id,omitempty" bson:"id,omitempty"`
	Name string        `json:"name,omitempty" bson:"name,omitempty"`
}

type Product struct {
	ID                  bson.ObjectId     `json:"id,omitempty" bson:"id,omitempty"`
	Name                string            `json:"name,omitempty" bson:"name,omitempty"`
	Identifier          string            `json:"identifier,omitempty" bson:"name,omitempty"`
	Categories          []ProductCategory `json:"categories,omitempty" bson:"categories,omitempty"`
	Position            int
	PositionDescription string
}
