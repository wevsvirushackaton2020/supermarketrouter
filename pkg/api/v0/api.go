package v0

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/api/v0/handlers"
	"gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/repository"
	globalServices "gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/services"
)

// API represents tne v0 implementation of the api
type API struct {
	MarketDB   repository.MarketDB
	ProductsDB repository.ProductsDB
	Sanitizer  globalServices.Sanitizer
}

// Register registers paths for v0
func (a *API) Register(g *echo.Group) {
	shoppingListHandler := handlers.NewShoppingListHandler(a.ProductsDB, a.MarketDB, a.Sanitizer)
	shoppingListHandler.Register(g.Group("/shoppingList"))

	handler := handlers.NewMarketHandler(a.MarketDB)
	g.POST("/addMarket", handler.AddMarket)
	g.POST("/addLocationToMarket", handler.AddLocationToMarket)
	g.POST("/findMarket", handler.FindMarket)
	g.POST("/calculateRoute", handler.CalculateRoute)
}
