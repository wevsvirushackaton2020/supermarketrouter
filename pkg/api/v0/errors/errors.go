package errors

import (
	"fmt"

	e "github.com/pkg/errors"
)

// UserError is the base class for errors
type UserError struct {
	error
	Message string
	Root    error
}

// NewUserError returns new Transform error that can be used as cause and still contain an error
func NewUserError(root error) UserError {
	return UserError{
		Root: root,
	}
}

// NewUserError returns new Transform error that can be used as cause and still contain an error
func NewUserErrorWithMessage(root error, format string, args ...interface{}) UserError {
	return UserError{
		Message: fmt.Sprintf(format, args...),
		Root:    root,
	}
}

// NewUserErrorf returns new Transform error that can be used as cause and still contain an error
func NewUserErrorf(format string, args ...interface{}) UserError {
	return UserError{
		Root: e.Errorf(format, args...),
	}
}

// Error returns the Root error
func (e UserError) Error() string {
	return e.Root.Error()
}
