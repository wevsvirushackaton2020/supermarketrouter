package services

import (
	"testing"

	"github.com/labstack/gommon/log"
	"gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/api/v0/models"
	"gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/configuration"
	"gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/repository"
)

func TestCalculateRoute(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping Integrationtest")
	}

	db, err := repository.NewMarketDB(configuration.LoadConfiguration())
	if err != nil {
		t.Errorf("Failed to create marketDB: %+v", err)
		t.Fail()
	}

	marketService := NewMarketService(db)

	address := models.Address{
		Street:      "Wolfhager Str.",
		HouseNumber: "7",
		ZipCode:     "36123",
		City:        "Kassel",
	}
	market := models.Market{
		Address: address,
		Locations: []models.Location{
			models.Location{
				Position:            2,
				PositionDescription: "Second shelf, right hand side",
				Category: models.ProductCategory{
					Name: "Fruits",
				},
			},
			models.Location{
				Position:            1,
				PositionDescription: "First shelf, left hand side",
				Category: models.ProductCategory{
					Name: "Dairy Products",
				},
			},
			models.Location{
				Position:            16,
				PositionDescription: "Last shelf before checkout, right hand side",
				Category: models.ProductCategory{
					Name: "Alcohol",
				},
			},
		},
	}

	shoppingList := models.CategorizedShoppingList{
		Products: []models.Product{
			models.Product{
				Name: "Tomato",
				Categories: []models.ProductCategory{
					models.ProductCategory{
						Name: "Fruits",
					},
				},
			},
			models.Product{
				Name: "Vodka",
				Categories: []models.ProductCategory{
					models.ProductCategory{
						Name: "Alcohol",
					},
				},
			},
			models.Product{
				Name: "Milk",
				Categories: []models.ProductCategory{
					models.ProductCategory{
						Name: "Dairy Products",
					},
				},
			},
			models.Product{
				Name: "Flour",
				Categories: []models.ProductCategory{
					models.ProductCategory{
						Name: "Baking Products",
					},
				},
			},
			models.Product{
				Name: "Bananas",
				Categories: []models.ProductCategory{
					models.ProductCategory{
						Name: "Fruits",
					},
				},
			},
		},
	}

	route, err := marketService.CalculateRoute(shoppingList, market)
	if err != nil {
		log.Error(err)
		t.Fail()
	}
	if len(route.Products) != len(shoppingList.Products) {
		log.Errorf("Route has different lenght (%d) than original shopping list (%d)", len(route.Products), len(shoppingList.Products))
		t.Fail()
	}
	if route.Products[0].Name != "Milk" {
		log.Errorf("First item in route should be Milk, but is %s", route.Products[0].Name)
		t.Fail()
	}
	if route.Products[1].Name != "Bananas" {
		log.Errorf("Second item in route should be Bananas, but is %s", route.Products[1].Name)
		t.Fail()
	}
	if route.Products[2].Name != "Tomato" {
		log.Errorf("Third item in route should be Tomato, but is %s", route.Products[2].Name)
		t.Fail()
	}
	if route.Products[3].Name != "Vodka" {
		log.Errorf("Fourth item in route should be Vodka, but is %s", route.Products[3].Name)
		t.Fail()
	}
	if route.Products[4].Name != "Flour" {
		log.Errorf("Last item in route should be Flour, but is %s", route.Products[4].Name)
		t.Fail()
	}
}
