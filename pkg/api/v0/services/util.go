package services

import "gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/api/v0/models"

// CategoriesContain returns, whether a slice of models.ProductCategory contains an entry with the same name
// as the given models.ProductCategory
func CategoriesContain(categories []models.ProductCategory, category models.ProductCategory) bool {
	for _, cat := range categories {
		if cat.Name == category.Name {
			return true
		}
	}
	return false
}

// SortintoRoute inserts a product into an existing route depending on its position
// @return Returns the sortedRoute of length `len(route) + 1` with the new item inserted at the correct position
func SortIntoRoute(route []models.Product, product models.Product) []models.Product {
	// if the route is empty, just add the new product
	if len(route) == 0 {
		route = append(route, product)
		return route
	}
	// if the new products position is less or equal to the first prudct in the route, append it to the head
	if product.Position <= route[0].Position {
		route = append([]models.Product{product}, route...)
		return route
	}
	// if the new products position is greater or equal to the last prudct in the route, append it to the tail
	if route[len(route)-1].Position <= product.Position {
		route = append(route, product)
		return route
	}
	// if the new products position is somewhere in between, find the correct position to insert the new product and shift all succeeding products accordingly
	sortedRoute := []models.Product{}
	for i, pos := range route {
		if pos.Position >= product.Position {
			// copy all products up to i (exclusive) into the sorted route
			sortedRoute = append(sortedRoute, route[:i]...)

			// insert new product
			sortedRoute = append(sortedRoute, product)

			// append rest of the original route
			sortedRoute = append(sortedRoute, route[i:]...)
			break
		}
	}
	return sortedRoute
}
