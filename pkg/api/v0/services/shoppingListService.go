package services

import (
	"github.com/pkg/errors"
	customErrors "gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/api/v0/errors"
	"gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/api/v0/models"
	"gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/repository"
	"gopkg.in/mgo.v2/bson"

	globalServices "gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/services"
)

type ShoppingListService interface {
	CategorizeShippingList(models.ShoppingList, bson.ObjectId) (models.CategorizedShoppingList, error)
}

type shoppingListService struct {
	productsDB repository.ProductsDB
	sanitizer  globalServices.Sanitizer
}

func NewShoppinglistService(productsDB repository.ProductsDB, sanitizer globalServices.Sanitizer) ShoppingListService {
	return &shoppingListService{
		productsDB: productsDB,
		sanitizer:  sanitizer,
	}
}

func (s *shoppingListService) CategorizeShippingList(shoppinglist models.ShoppingList, marketID bson.ObjectId) (models.CategorizedShoppingList, error) {
	result := models.CategorizedShoppingList{
		Products: make([]models.Product, len(shoppinglist.Items)),
	}

	for i, item := range shoppinglist.Items {
		identifier, err := s.sanitizer.SanitizeShoppingListItem(item)
		if err != nil {
			return models.CategorizedShoppingList{}, customErrors.NewUserErrorWithMessage(err, "error while trying to sanitize an item: %s", item)
		}

		result.Products[i], err = s.productsDB.GetProductByIdentifier(identifier)
		if err != nil {
			// TODO: skip?
			return models.CategorizedShoppingList{}, errors.Wrapf(err, "item not Found %s in Market %s", identifier, string(marketID))
		}
	}

	return result, nil
}
