package services

import (
	"github.com/labstack/gommon/log"
	"gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/api/v0/models"
	"gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/repository"
	"gopkg.in/mgo.v2/bson"
)

type MarketService interface {
	FindMarket(market models.Market) ([]models.Market, error)
	AddLocationToMarket(models.Location, models.Market) error
	AddMarket(models.Market) error
	CalculateRoute(shoppingList models.CategorizedShoppingList, market models.Market) (models.CategorizedShoppingList, error)
}

type marketService struct {
	MarketDB repository.MarketDB
}

func NewMarketService(marketDB repository.MarketDB) MarketService {
	return &marketService{
		MarketDB: marketDB,
	}
}

func (m *marketService) FindMarket(market models.Market) ([]models.Market, error) {
	var markets []models.Market
	markets, err := m.MarketDB.FindMarket(bson.M{"address": market.Address})
	return markets, err
}

func (m *marketService) AddLocationToMarket(location models.Location, market models.Market) error {
	return m.MarketDB.AddLocationToMarket(location, market)
}

func (m *marketService) AddMarket(market models.Market) error {
	return m.MarketDB.AddMarket(market)
}

func (m *marketService) CalculateRoute(shoppingList models.CategorizedShoppingList, market models.Market) (models.CategorizedShoppingList, error) {
	route := []models.Product{}
	for _, product := range shoppingList.Products {
		matchedCategory := false
		for _, location := range market.Locations {
			category := location.Category
			if len(category.Name) == 0 {
				// get category from set product instead
				// TODO: handle more than one category here
				category = location.Product.Categories[0]
			}
			// if there still is no category, continue
			if len(category.Name) == 0 {
				log.Warnf("No category in location: %+v", location)
				continue
			}
			if CategoriesContain(product.Categories, category) {
				// save postion info for this product
				product.Position = location.Position
				product.PositionDescription = location.PositionDescription
				matchedCategory = true
				break
			}
		}
		if !matchedCategory {
			// if no location matched the product, we append it to the end of the list as a fallback
			product.Position = 999
			product.PositionDescription = "UNKNOWN"
		}

		// save updated product to route
		route = SortIntoRoute(route, product)
	}
	shoppingList.Products = route
	return shoppingList, nil
}
