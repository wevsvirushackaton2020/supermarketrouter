package services

import (
	"fmt"
	"testing"

	"gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/api/v0/models"
	"gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/configuration"
	"gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/repository"
	"gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/services"
	"gopkg.in/mgo.v2/bson"
)

type san struct{}
type productsDB struct{}

func (s san) SanitizeShoppingListItem(i string) (string, error) {
	return map[string]string{
		"Milch":    "#milk",
		"Bier":     "#beer",
		"Brot":     "#loaf",
		"Zwiebeln": "#onion",
		"Salat":    "#salad",
		"Tomaten":  "#tomato",
		"Eier":     "#eggs",
		"Käse":     "#chees",
		"Äpfel": "#appl"}[i], nil
}

func (pdb productsDB) GetProductByIdentifier(identifier string) (models.Product, error) {
	identifierToProductname := map[string]string{
		"#milk":   "Milch",
		"#beer":   "Bier",
		"#loaf":   "Brot",
		"#onion":  "Zwiebeln",
		"#salad":  "Salat",
		"#tomato": "Tomaten",
		"#eggs":   "Eier",
		"#chees":  "Käse",
		"#appl": "Äpfel"}

	return models.Product{
		Identifier: identifier,
		Name:       identifierToProductname[identifier],
	}, nil
}

func (pdb productsDB) Reconnect() error {
	return nil
}

func newSanitizer() services.Sanitizer {
	return san{}
}

func newProductsDb() repository.ProductsDB {
	return productsDB{}
}

func Test_ShoppingList(t *testing.T) {
	s := newSanitizer()
	pdb := newProductsDb()

	sls := NewShoppinglistService(pdb, s)

	input := models.ShoppingList{
		Items: []string{
			"Milch",
			"Bier",
			"Brot",
			"Zwiebeln",
			"Salat",
			"Tomaten",
			"Eier",
			"Käse",
			"Äpfel"},
	}

	categorizedShoppingList, err := sls.CategorizeShippingList(input, bson.ObjectId("da5157317357"))

	if err != nil {
		fmt.Println("error occurred while categorizing the List")
		t.Fail()
	}

	if len(categorizedShoppingList.Products) != len(input.Items) {
		fmt.Println("wrong amount of items")
		t.Fail()
	}

	for i, element := range categorizedShoppingList.Products {
		if element.Name != input.Items[i] {
			t.Fail()
		}
	}
}

func Test_ShoppingList_Integration(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping Integrationtest")
	}

	s := services.NewSanitizer()
	pdb, err := repository.NewProductsDB(configuration.Configuration{
		SSL:        false,
		DBHost:     "localhost",
		DBName:     "supermarketRouter",
		DBPassword: "",
		DBUsername: "",
	})

	if err != nil {
		t.Fail()
	}

	sls := NewShoppinglistService(pdb, s)

	input := models.ShoppingList{
		Items: []string{
			"Milch",
			"Bier",
			"Brot",
			"Zwiebeln",
			"Salat",
			"Tomaten",
			"Eier",
			"Käse",
			"Äpfel"},
	}

	categorizedShoppingList, err := sls.CategorizeShippingList(input, bson.ObjectId("da5157317357"))

	if err != nil {
		fmt.Println("error occurred while categorizing the List")
		t.Fail()
	}

	if len(categorizedShoppingList.Products) != len(input.Items) {
		fmt.Println("wrong amount of items")
		t.Fail()
	}

	for i, element := range categorizedShoppingList.Products {
		if element.Name != input.Items[i] {
			t.Fail()
		}
	}
}
