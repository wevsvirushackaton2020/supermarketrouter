package handlers

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
	"gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/api/v0/models"
	"gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/api/v0/services"
	"gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/repository"
)

// MarketHandler handles requests regarding the market
type MarketHandler interface {
	FindMarket(echo.Context) error
	AddLocationToMarket(echo.Context) error
	AddMarket(echo.Context) error
	CalculateRoute(echo.Context) error
}

type marketHandler struct {
	MarketService services.MarketService
}

// NewMarketHandler returns new Handler for market routes
func NewMarketHandler(marketDB repository.MarketDB) MarketHandler {
	marketService := services.NewMarketService(marketDB)
	return &marketHandler{
		MarketService: marketService,
	}
}

func (m *marketHandler) FindMarket(c echo.Context) error {
	var market models.Market
	err := c.Bind(&market)
	if err != nil {
		log.Error(err)
		return err
	}
	markets, err := m.MarketService.FindMarket(market)
	if err != nil {
		log.Error(err)
		return err
	}
	return c.JSON(200, markets)
}

func (m *marketHandler) AddLocationToMarket(c echo.Context) error {
	var requestData struct {
		Location models.Location
		Market   models.Market
	}
	err := c.Bind(&requestData)
	if err != nil {
		log.Error(err)
		return err
	}
	err = m.MarketService.AddLocationToMarket(requestData.Location, requestData.Market)
	if err != nil {
		log.Error(err)
		return err
	}
	return c.String(200, "ok")
}

func (m *marketHandler) AddMarket(c echo.Context) error {
	var market models.Market
	err := c.Bind(&market)
	if err != nil {
		log.Error(err)
		return err
	}
	err = m.MarketService.AddMarket(market)
	if err != nil {
		log.Error(err)
		return err
	}
	return c.String(200, "ok")
}

func (m *marketHandler) CalculateRoute(c echo.Context) error {
	var requestData struct {
		Market       models.Market
		ShoppingList models.CategorizedShoppingList
	}
	err := c.Bind(&requestData)
	if err != nil {
		log.Error(err)
		return err
	}
	route, err := m.MarketService.CalculateRoute(requestData.ShoppingList, requestData.Market)
	if err != nil {
		log.Error(err)
		return err
	}
	return c.JSON(200, route)
}
