package handlers

import (
	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/api/handler"
	"gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/api/v0/models"
	"gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/api/v0/services"
	"gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/repository"
	globalServices "gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/services"
	"gopkg.in/mgo.v2/bson"

	customErrors "gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/api/v0/errors"
)

// ShoppingList handles requests regarding the shoppingList
type ShoppingList interface {
	Register(g *echo.Group)
}

type shoppingList struct {
	service services.ShoppingListService
}

// NewShoppingListHandler returns new Handler for shoppingList routes
func NewShoppingListHandler(productsDB repository.ProductsDB, marketDB repository.MarketDB, sanitizer globalServices.Sanitizer) ShoppingList {
	service := services.NewShoppinglistService(productsDB, sanitizer)

	return &shoppingList{
		service: service,
	}
}

func (l *shoppingList) Register(g *echo.Group) {
	g.POST("/categorize", l.categorize)
}

func (l *shoppingList) categorize(c echo.Context) error {
	cContext := c.(*handler.CContext)

	marketID := bson.ObjectId(c.QueryParams().Get("marketId"))

	shoppingList := models.ShoppingList{}
	if err := c.Bind(&shoppingList); err != nil {
		cContext.Log("Error while parsing request", logrus.WarnLevel)
		return err
	}

	categorizedShoppinglist, err := l.service.CategorizeShippingList(shoppingList, marketID)

	if err != nil {
		switch cause := errors.Cause(err).(type) {
		case customErrors.UserError:
			cContext.LogErr("An error occurred while trying to categorize a shoppingList", err, logrus.WarnLevel)
			userError := customErrors.UserError(cause)
			// TODO: get the responsecode from the error instead of the default 400
			return cContext.JSON(400, struct {
				Message string `json:"message"`
			}{
				Message: userError.Root.Error(),
			})
		}

		err := errors.Wrap(err, "Internal Server error while categorizing a shoppinglist")
		cContext.LogErr("Internal Server error while categorizing a shoppinglist", err, logrus.ErrorLevel)
		return err
	}

	return c.JSON(200, categorizedShoppinglist)
}
