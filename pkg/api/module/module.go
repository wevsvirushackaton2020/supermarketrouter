package module

import "github.com/labstack/echo/v4"

// Module is the base interface for REST modules
type Module interface {
	Register(*echo.Group)
}
