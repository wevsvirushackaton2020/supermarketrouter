package logger

import (
	"github.com/fino-digital/sematextHook"
	"github.com/go-resty/resty/v2"
	"github.com/labstack/echo/v4"
	echolog "github.com/labstack/gommon/log"
	"github.com/neko-neko/echo-logrus/v2/log"
)

// InitLogger initialize logger
func InitLogger(e *echo.Echo, serviceName, group, sematextURL, environment string, isDev bool) {
	logger := log.Logger()
	logger.SetLevel(echolog.INFO)

	if isDev {
		logger.SetLevel(echolog.DEBUG)
	}

	hook, err := sematextHook.NewSematextHook(resty.New(), sematextURL, group, serviceName, environment)

	if err != nil {
		logger.WithError(err).Warn("Failed to initialize sematext hook")
		return
	}

	e.Logger = logger
	logger.AddHook(hook)
	e.Use(sematextHook.AccessLog())
}
