package logger

// customError hold external and internal error message
type customError struct {
	internalError   error
	externalMessage string
}

func (err *customError) Error() string {
	return "external message: " + err.externalMessage + "; internal error: " + err.internalError.Error()
}

func cError(externalMessage string, err error) error {
	return &customError{
		externalMessage: externalMessage,
		internalError:   err,
	}
}

// logfunc to call specific log method
type logfunc func(i ...interface{})

// Log can be used to log a message
func Log(externalMessage string, err error, method logfunc) {
	method(cError(externalMessage, err))
}
