package services

import (
	"strings"
	"testing"
)

func Test_stemmer(t *testing.T) {
	s := NewSanitizer()
	b, err := s.SanitizeShoppingListItem("Äpfel")
	if err != nil {
		t.Fail()
	}

	if !strings.EqualFold(b, "#appl") {
		t.Fail()
	}
}
