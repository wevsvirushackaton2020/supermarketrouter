package services

import (
	"fmt"
	"strings"

	stemmer "github.com/antonbaumann/german-go-stemmer"
	"github.com/pkg/errors"
	userError "gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/api/v0/errors"
)

type sanitizer struct {
	translator *translator
}

// Sanitizer  can be used to sanitize items on a shoppinglist
type Sanitizer interface {
	SanitizeShoppingListItem(string) (string, error)
}

// NewSanitizer returns a new instance if the Sanitizer interface
func NewSanitizer() Sanitizer {
	return &sanitizer{
		translator: &translator{},
	}
}

// to lower
// determine language
// translate to english
// translatable magic
func (s *sanitizer) SanitizeShoppingListItem(item string) (string, error) {
	var err error

	switch s.translator.getLanguage(item) {
	case langDE:
		item, err = s.translator.translateToEnglish(item, langDE)
		if err != nil {
			return item, errors.Wrapf(err, "error while translating to english: '%s'", item)
		}

	case langEN:
	default:
		return "", userError.NewUserErrorf("unsupported Language on item: '%s'", item)
	}

	item = stemmer.StemWord(item)
	item = strings.ToLower(item)
	return fmt.Sprintf("#%s", item), nil
}
