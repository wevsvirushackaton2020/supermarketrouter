package services

import (
	"fmt"

	gt "github.com/wr4thon/googletranslatefree"
)

type lang string

const (
	langDE lang = "de"
	langEN lang = "en"
)

// Translator can be used to get the tranlation tag of an item
// or to translate an item translation tag into the desired locale
type Translator interface {
	GetIdentifier(string) (string, error)
}

type translator struct {
}

// NewTranslator returns a new instance if the Translator interface
func NewTranslator() Translator {
	return &translator{}
}

func (t *translator) GetIdentifier(item string) (string, error) {
	return fmt.Sprintf("#%s", item), nil
}

func (t *translator) translateToEnglish(item string, l lang) (string, error) {
	return gt.Translate(item, string(l), "en")
}

func (t *translator) getLanguage(item string) lang {
	return langDE
}
