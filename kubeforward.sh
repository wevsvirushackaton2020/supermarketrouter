#!/bin/bash

kubectl -n marketrouter port-forward $(kubectl -n marketrouter get pods |grep mongodb|tail -1|awk '{print $1}') 27117:27017