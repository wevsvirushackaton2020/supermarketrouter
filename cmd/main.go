package main

import (
	"net/http"
	"os"

	"github.com/gofrs/uuid"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"
	"github.com/labstack/gommon/random"
	"gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/api"
	"gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/api/handler"
	"gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/configuration"
	"gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/logger"
	"gitlab.com/wevsvirushackaton2020/supermarketrouter/pkg/repository"
)

func main() {
	// Echo instance
	port := ":8080"
	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(customContextMiddleware)
	e.Use(uuidRequestID())

	// TODO: use sematext?
	logger.InitLogger(e, "", "", "", "", false)

	e.GET("/ping", ping)
	e.GET("/version", version)

	marketDB, err := repository.NewMarketDB(configuration.LoadConfiguration())
	if err != nil {
		log.Fatal(err)
	}

	productsDB, err := repository.NewProductsDB(configuration.LoadConfiguration())
	if err != nil {
		log.Fatal(err)
	}

	a := api.NewV0(marketDB, productsDB)
	v0 := e.Group("/v0")
	a.Register(v0)

	e.Logger.Fatal(e.Start(port))
}

func customContextMiddleware(handlerFunc echo.HandlerFunc) echo.HandlerFunc {
	return func(context echo.Context) error {
		cContext := &handler.CContext{
			Context: context,
		}
		return handlerFunc(cContext)
	}
}

func uuidRequestID() echo.MiddlewareFunc {
	return middleware.RequestIDWithConfig(middleware.RequestIDConfig{
		Generator: func() string {
			randomUUID, err := uuid.NewV4()
			if err != nil {
				return random.String(32)
			}
			return randomUUID.String()
		},
	})
}

func version(c echo.Context) error {
	return c.String(http.StatusOK, os.Getenv("VERSION"))
}

func ping(c echo.Context) error {
	return c.String(http.StatusOK, "pong")
}
