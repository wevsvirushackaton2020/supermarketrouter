#!/bin/bash

export VERSION=$1
[ -z $VERSION ] && export VERSION=$(git describe --tags|sed 's/v//')
envsubst < ./config/deployment.yaml | kubectl apply -f -