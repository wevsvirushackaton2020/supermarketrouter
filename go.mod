module gitlab.com/wevsvirushackaton2020/supermarketrouter

go 1.14

require (
	github.com/antonbaumann/german-go-stemmer v1.2.0
	github.com/fino-digital/sematextHook v0.0.0-20190919114127-1c557b4ea878
	github.com/go-resty/resty/v2 v2.2.0
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/labstack/echo/v4 v4.1.15
	github.com/labstack/gommon v0.3.0
	github.com/neko-neko/echo-logrus/v2 v2.0.1
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.4.2
	github.com/wr4thon/googletranslatefree v0.0.0-20200322202117-bad06ea658d1
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
)
