#!/bin/bash

[ -z $1 ] && export VERSION=$(git describe --tags|sed 's/v//')

echo "########################"
echo "building version: $VERSION"
echo "########################"
echo 

docker build . -t "docker.io/wr4thon/market-router:$VERSION" && \
docker image push "docker.io/wr4thon/market-router:$VERSION" && \
source ./kubeApply.sh $VERSION