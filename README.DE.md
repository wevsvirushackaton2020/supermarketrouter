# Supermarket Router

## Andere Sprachen

- [German](README.DE.md)
- [English](README.md)

## Inhalt

- [Beschreibung](Beschreibung)
- [Ablauf/Nutzung](Ablauf/Nutzung)
- [Dokumentation](Dokumentation)

## Beschreibung

Das Ziel beim Entwickeln des Services war, dass wir anhand einer Einkaugsliste die optimale route durch einen Supermarkt errechnen. Durch das hochladen einer Einkaufsliste und das angeben des zu besuchenden Marktes errechnen wir die Effizienteste Route durch den Markt, basierend auf den Daten, die uns von dem Markt vorliegen.

## Ablauf/Nutzung

1. Lege Markt mit adresse an
2. Füge die Produktkategorien (oder auch einzel-Produkte) mit Position hinzu:
  zB. {1 Obst}, {2 Nudeln}, {5 Wasser}, ...
3. Lade Einkaufsliste hoch
4. Kategorsiere Listeneinträge mit ProduktKategorien
5. Suche diese Kategorien im gewählten markt und ordne liste entsprechend
6. Schicke sortierte liste zurück (evtl mit groben richtungsangaben: "1. regal rechts")

## Dokumentation

Die Dokumentation ist noch in Arbeit. Wenn wir entschieden haben, dass wir zufrieden sind, wie die API aussieht, wird eine Swagger Dokumentation nachgereicht.
