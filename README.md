# Supermarket Router

## languages

- [German](README.DE.md)
- [English](README.md)

## index

- [Description](Description)
- [How to use](How_to_use)
- [Documentation](Documentation)

## Description

we tried to develop a service that will give you the optimal route for your shopping trip based on the shopping list you provided and the data we have for that market

## How to use

1. Create a new Market
2. Add product categories or products and their locations within the market

   eg. Fruits, Noodles, Water, ...

3. Upload a shoppinglist
4. get back an ordered List sorted by locations in the market to get the best route for the list based on the data we have for that market

## Documentation

this is still in Progress. when we have decided that we are done with the API we will provide a swaggwer documentation
